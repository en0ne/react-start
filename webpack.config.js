const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const merge = require('webpack-merge');

const TARGET = process.env.npm_lifecycle_event;

const common = {
    resolve: {
        alias: {
            app: path.resolve(__dirname, 'src/'),
        },
        mainFiles: ['index']
    },
    entry: {
        app: './src/index.js',
    },
    output: {
        filename: 'bundle/[name]-[hash].js',
        path: path.resolve(__dirname, 'public'),
        chunkFilename: 'bundle/[name]-[hash].js',
        publicPath: '/',
    },
    module: {
        rules: [{
            test: /\.jsx*$/,
            use: ['babel-loader'],
            exclude: /node_modules/
        }, {
            test: /\.css$/,
            use: ['style-loader', 'css-loader'],
        }, {
            test: /\.(png|jpg|gif)$/,
            exclude: /(node_modules)/,
            use: [{
                loader: 'file-loader'
            }]
        }]
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'react-start',
            filename: 'index.html',
            template: './src/index.html'
        })
    ]
};

module.exports = common;

if (TARGET === 'start') {
    module.exports = merge(common, {
        mode: 'development',
        devtool: 'cheap-module-source-map',
        devServer: {
            host: 'localhost',
            port: 15732,
            open: true,
            publicPath: '/',
            contentBase: false,
            historyApiFallback: true
        },
    });
}

if (TARGET === 'build') {
    module.exports = merge(common, {
        mode: 'production',
    });
}