import { observable } from 'mobx';

class User {
    @observable id = null;
    @observable email = null;
}


export default class ExampleStore {
    @observable users = [];

    constructor(rootStore) {
        this.rootStore = rootStore;

        // just for example
        const user1 = new User();
        user1.id = 1;
        user1.email = 'user1@mail.ru';

        this.users.push(user1);

        const user2 = new User();
        user2.id = 2;
        user2.email = 'user2@mail.ru';

        this.users.push(user2);
    }

}