import ExampleStore from './ExampleStore';

class RootStore {
    constructor() {
        this.exampleStore = new ExampleStore();
    }
}

export default new RootStore();