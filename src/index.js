import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'mobx-react'
import Routes from './Routes'
import rootStore from 'app/stores/RootStore'

ReactDOM.render(
    <Provider {...rootStore}>
        <Routes/>
    </Provider>,
    document.getElementById('root')
);
