import React from 'react'
import history from 'app/history'
import { Route, Router, Switch } from 'react-router-dom'
import { ROUTE_EXAMPLE, ROUTE_HOME, routeByCode } from 'app/RoutesMap'

import { NotFoundPage, HomePage, ExamplePage } from 'app/pages'

export default class Routes extends React.Component {
    render() {
        return (
            <Router history={history}>
                <Switch>
                    <Route path={routeByCode(ROUTE_HOME)} component={HomePage} exact/>
                    <Route path={routeByCode(ROUTE_EXAMPLE)} component={ExamplePage} exact/>
                    <Route component={NotFoundPage}/>
                </Switch>
            </Router>
        );
    }
}