export const ROUTE_HOME = 'ROUTE_HOME';
export const ROUTE_EXAMPLE = 'ROUTE_EXAMPLE';

export const RoutesMap = {
    ROUTE_HOME: '/',
    ROUTE_EXAMPLE: '/example'
};

export const routeByCode = (routeCode) => (
    RoutesMap[routeCode]
);

export const urlFor = (routeCode, params) => {
    let url = routeByCode(routeCode);
    for (const key of Object.keys(params || {})) {
        url = url.replace(':' + key, params[key])
    }
    return url;
};