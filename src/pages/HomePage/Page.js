import React from 'react';
import { Link } from 'react-router-dom';
import { ROUTE_EXAMPLE, urlFor } from 'app/RoutesMap';

export default class Page extends React.Component {
    render() {
        return (
            <div>
                <h2>Home Sweet Home</h2>
                <Link to={urlFor(ROUTE_EXAMPLE)}>Example</Link>
            </div>
        );
    }
}