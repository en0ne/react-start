import React from 'react'
import { inject, observer } from 'mobx-react'
import cn from 'classnames';
import './css/styles.css'

@inject('exampleStore')
@observer
export default class Page extends React.Component {
    render() {
        const {exampleStore} = this.props;
        const {users} = exampleStore;
        return (
            <div>
                <h2>Users</h2>
                <ul className={cn('user-list')}>
                    {users.map(user => (
                        <li key={user.id}>{user.email}</li>
                    ))}
                </ul>
            </div>
        );
    }
}